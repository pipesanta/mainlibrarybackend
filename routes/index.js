"use strict";

const AuthorizationRoutes = require("./Authorization");
const BookRoutes = require("./Books");

module.exports = {
  AuthorizationRoutes,
  BookRoutes
};
