'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');


function isAuth(req, res, next){
    console.log("PASANDO POR EL MIDDLE DE AUTH");
    
    const authorization = req.headers.authorization;
    console.log({ authorization });
    if(!authorization){
        return res.status(403).send({ message: "YOU DONT HAVE AUTHORIZATION"})
    }
    const token = authorization.split(" ")[1];
    const payload = jwt.decode(token, process.env.SECRET_TOKEN_KEYS);
    console.log({ payload });
    
    if(payload.exp < Date.now()){
        console.log("EXPIRED");
        
        return res.status(401).send({message: "token expired"})

    }
    req.user = payload.sub;
    next()

}

module.exports = {
    isAuth
}