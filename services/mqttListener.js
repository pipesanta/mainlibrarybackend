'use strict'
const { Observable, bindCallback, bindNodeCallback, of, throwError } = require("rxjs")
const { mergeMap, tap, catchError } = require("rxjs/operators");
// const { mqttIntance } = require("../tools/mqtt");
const { NeDB } = require('../tools/NeDB');
const { sendMessage } = require("../services/webSocket/webSockets");

class MqttListener{

    start$() {
        console.log('on -------- MqttListener.start$');
        return Observable.create(observer => {
    
            mqttIntance.listenEvents$().pipe(
                mergeMap(event => MqttListener.completeEventData$(event)),
                tap(r => console.log(r)),
                mergeMap(ce => bindCallback(NeDB.eventsCollection.insert.bind(NeDB.eventsCollection))(ce) ),
                mergeMap(([error, result]) => {
                    if(error) return throwError("operation failed");
                    return of(result);
                }),
                catchError(e => {
                    console.log(e);
                    return of(null);
                })
            ).subscribe(
                (event) => { 
                    sendMessage(JSON.stringify(event))
                },
                (e) => console.error(e),
                () => { console.log("TERMINATED!!! ") }
            )
          
    
          observer.complete();
        });
      }



      static completeEventData$(rawEvent){
          console.log(rawEvent);

          
        let query = null;
        const queryMap = {
            "SL500L": { cardId: rawEvent.cardId },
            "WS8552": { ws8552UserId: rawEvent.user_id }
        }
        query = queryMap[rawEvent.sensor];        

        if(!query) return throwError("sensor not allowed");
        return bindCallback( NeDB.usersCollection.findOne.bind(NeDB.usersCollection))(query).pipe(
            mergeMap(([error, result]) => {
                if(error) return of(null);
                const { name, lastname, cardId, fingerprint } = result || { name: 'unknow', lastname: '', cardId: rawEvent.cardId }
                return of({...rawEvent, name, lastname, cardId, fingerprint });
            }),
        )
      }

}





module.exports={
    start$: new MqttListener().start$

}
